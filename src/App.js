import Main from "./Core/Main";
import '../src/assets/scss/main.scss'
import 'normalize.css'


function App() {
  return (
    <div>
      <Main/>
    </div>
  );
}

export default App;
