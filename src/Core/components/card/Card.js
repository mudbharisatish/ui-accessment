import React from "react";

const Card = (props) => {
  return (
    <div>
      <div className="row">
        <div className="col-md-4">
          <div className={`card cardbg-white`}>
            <div className="img-fluid">
              <img src="../../../assets/img/Vector.png" alt="vector" />
            </div>
            <div>
              <p className="title">Compounding Magic SIP</p>
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <div className={`card cardbg-white`}>
            <div className="img-fluid">
              <img src="../../../assets/img/search.png" alt="search" />
            </div>
            <div>
              <p className="title">Compounding Magic Lumpsum</p>
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <div className={`card cardbg-blue`}>
            <div className="img-fluid">
              <img src="../../../assets/img/calculate.png" alt="calculate" />
            </div>
            <div>
              <p className="title text-white">SPI Returns Calculator</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
