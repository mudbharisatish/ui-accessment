import React, { Component } from "react";
import { Chart, registerables } from "chart.js";
import { LinearScale, LineController, LineElement } from "chart.js";

export class Line extends Component {
  chartRef = React.createRef();
  componentDidMount() {
    const myChartRef = this.chartRef.current.getContext("2d");
    Chart.register(...registerables);
    new Chart(myChartRef, {
      type: "line",
      data: {
        labels: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec",
        ],
        datasets: [
          {
            label: "# of Votes",
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: ["#37DE80"],
            borderColor: ["#37DE80"],
            borderWidth: 1,
          },
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          title: {
            display: true,
            text: this.props.text,
            color: "#37DE80",
            padding: "10",
            align: "center",
          },
          labels: {
            color: "#37DE80",
          },
          legend: {
            display: false,
          },
        },
        scales: {
          x: {
            ticks: {
              color: "#37DE80",
              fontSize: 14,
            },
          },
          y: {
            ticks: {
              color: "#37DE80",
              fontSize: 14,
            },
          },
          innerHeight: "200px",
        },
      },
    });
  }
  render() {
    return (
      <div>
        <canvas id="my-chart" ref={this.chartRef} width="500"></canvas>
      </div>
    );
  }
}

export default Line;
