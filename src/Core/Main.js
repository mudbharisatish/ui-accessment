import React from "react";
import logo from "../assets/img/logo.svg";
import headerImage from "../assets/img/haribansa-and-madan.jpg";
import DownArrow from "../assets/img/down-arrow.png";
import Home from "../assets/img/sidebar-home.png";
import Search from "../assets/img/search.png";
import Setting from "../assets/img/sidebar-setting.png";
import Graph from "../assets/img/sidebar-bar.png";
import SingleSetting from "../assets/img/sidebar-single.png";
import Book from "../assets/img/sidebar-book.png";
import Contact from "../assets/img/sidebar-contact.png";
import styled from "styled-components";

import "../assets/scss/main.scss";

const Main = () => {
  return (
    <div>
      <header>
       

        
          <a className="navbar-brand" href="#">
            <img src={logo} />
          </a>

          <nav className="navbar  navbar-expand-lg navbar-light ">
            <div className="container-fluid ">
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNavDropdown">
                <ul className="navbar-nav">
                  <li className="nav-item">
                    <a className="nav-link active" aria-current="page" href="#">
                      Home
                    </a>
                  </li>

                  <li className="nav-item dropdown">
                    <a
                      className="nav-link active dropdown-toggle"
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      Our Offering
                    </a>
                    <ul
                      class="dropdown-menu"
                      aria-labelledby="navbarDropdownMenuLink"
                    >
                      <li>
                        <a class="dropdown-item" href="#">
                          Action
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="nav-item dropdown">
                    <a
                      className="nav-link active dropdown-toggle"
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      Who We Are
                    </a>
                    <ul
                      class="dropdown-menu"
                      aria-labelledby="navbarDropdownMenuLink"
                    >
                      <li>
                        <a class="dropdown-item" href="#">
                          Action
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="nav-item dropdown">
                    <a
                      className="nav-link active dropdown-toggle"
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      StakeHolders Information
                    </a>
                    <ul
                      class="dropdown-menu"
                      aria-labelledby="navbarDropdownMenuLink"
                    >
                      <li>
                        <a class="dropdown-item" href="#">
                          Action
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        
      </header>
      <main>
        <div className="graph">
          <img src={Graph} className="graph-img" />
        </div>
        <div className="home">
          <img src={Home} className="home-img" />
        </div>
        <div className="setting">
          <img src={Setting} className="setting-img" />
        </div>

        <div className="singlesetting">
          <img src={SingleSetting} className="singlesetting-img" />
        </div>
        <div className="book">
          <img src={Book} className="book-img" />
        </div>
        <div className="contact">
          <img src={Contact} className="contact-img" />
        </div>

        <div className="container">
          <section className="grid-column">
            <div>
              <h1 className="heading">Sanima Balanced Portfolio</h1>
              <h3 className="heading-h3 ">Your Trust, Our Assurance</h3>
              <p className="heading-text">
                Highest level of ethical standards, professional integrity,
                corporate governance, and regulatory compliance. Sanima Capital
                is committed to following the same path by introducing
                innovative products in Investment Banking space and serving its
                clients in a right way and with right values.
                <h3 className="learn-more">Learn More</h3>
              </p>
            </div>

            <div>
              <img src={headerImage} alt="headerImage" />
            </div>
          </section>
        </div>
      </main>
      <footer></footer>
    </div>
  );
};
export default Main;
