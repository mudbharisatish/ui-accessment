import React from "react";
import Card from "./components/card/Card";
// import { Line } from "react-chartjs-2";
import Line from "./Line";
const Content = () => {
  // const data = {
  //   labels: [
  //     "Jan",
  //     "Feb",
  //     "Mar",
  //     "Apr",
  //     "May",
  //     "Jun",
  //     "Jul",
  //     "Aug",
  //     "Sep",
  //     "Oct",
  //     "Nov",
  //     "Dec",
  //   ],
  //   datasets: [
  //     {
  //       data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
  //       borderColor: "#0D542C",
  //       borderWidth: 2,
  //       barThickness: 10,
  //     },
  //   ],
  // };

  // const options = {
  //   maintainAspectRatio: false,
  //   legend: {
  //     display: false,
  //   },
  //   labels: {
  //     fontColor: "blue",
  //     fontSize: 18,
  //   },
  //   borderRadius: 8,
  // };

  return (
    <>
      <section className="graph container ">
        <div className="stock-color"></div>
        <div className="stock"></div>
        <div className="Nav-title">
          <h4>NAV Graph of SAEF</h4>
        </div>
        <div className="row linegraph-1">
          <div className="col-md-6">
            <Line text="Sanima Equity Fund" />
          </div>
          <div className="col-md-6">
            <Line text="Sanima Large Cap Fund" />
          </div>
        </div>
      </section>
      <section className="container">
        <h4 className="tools">Useful Tools</h4>
        <p className="info">Build Your Wealth in Small Steps</p>
        <div className="inner ">
          <Card />
        </div>
      </section>
    </>
  );
};

export default Content;
